<?php

declare(strict_types=1);

namespace MondayFactory\Collections\Collection;

use MondayFactory\Collections\Iterator\ObjectIterator;

abstract class ImmutableObjectCollection extends ObjectIterator implements \Countable
{

	public function __construct(array $data)
	{
		foreach ($data as $item) {
			$this->validateItem($item);
		}

		parent::__construct($data);
	}


	/**
	 * @param  mixed $item
	 * @return mixed
	 */
	public function addItem($item)
	{
		$this->validateItem($item);

		return new static(array_merge($this->data, [$item]));
	}

	/**
	 * @param mixed $item
	 * @throws \InvalidArgumentException
	 */
	private function validateItem($item): void
	{
		$classItemName = $this->getItemType();

		if (!$item instanceof $classItemName) {
			throw new \InvalidArgumentException(static::class . '::addItem() only accepts ' . $this->getItemType());
		}
	}


	public function count(): int
	{
		return count($this->data);
	}


	public function isEmpty(): bool
	{
		return $this->data === [];
	}

	abstract protected function getItemType(): string;

}
