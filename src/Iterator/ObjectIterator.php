<?php

declare(strict_types=1);

namespace MondayFactory\Collections\Iterator;

class ObjectIterator implements \Iterator
{

	/**
	 * @var array|mixed[]
	 */
	protected $data;


	public function __construct(array $data)
	{
		$this->data = $data;
	}


	public function rewind(): void
	{
		reset($this->data);
	}


	/**
	 * @return int|string|null
	 */
	public function key()
	{
		return key($this->data);
	}


	public function next(): void
	{
		next($this->data);
	}


	public function valid(): bool
	{
		return key($this->data) !== null;
	}


	/**
	 * @return mixed
	 */
	public function current()
	{
		return current($this->data);
	}
}
